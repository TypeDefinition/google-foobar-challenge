import java.math.BigInteger;

public class Solution {
    public static String solution(String x, String y) {
        BigInteger m = new BigInteger(x);
        BigInteger f = new BigInteger(y);
        
        // Work backwards. Every generation, M = M + F or F = F + M. Therefore, the number that just got added MUST be the larger number.
        BigInteger generations = BigInteger.ZERO;
        while (true) {
            if (m.equals(BigInteger.ONE) && f.equals(BigInteger.ONE)) {
                return generations.toString();
            } else if (m.compareTo(BigInteger.ONE) < 0 || f.compareTo(BigInteger.ONE) < 0) {
                return "impossible";
            }
            
            int comp = m.compareTo(f);
            if (comp > 0) {
                BigInteger[] divAndRem = m.subtract(BigInteger.ONE).divideAndRemainder(f);
                generations = generations.add(divAndRem[0]);
                m = divAndRem[1].add(BigInteger.ONE);
            } else if (comp < 0) {
                BigInteger[] divAndRem = f.subtract(BigInteger.ONE).divideAndRemainder(m);
                generations = generations.add(divAndRem[0]);
                f = divAndRem[1].add(BigInteger.ONE);;
            } else {
                return "impossible";
            }
        }
    }
}