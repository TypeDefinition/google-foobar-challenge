import java.util.HashSet;

public class Solution {
    public static int solution(int[] x, int[] y) {
        HashSet<Integer> xIDs = new HashSet<Integer>();
        for (int i = 0; i < x.length; ++i) {
            xIDs.add(x[i]);
        }

        HashSet<Integer> yIDs = new HashSet<Integer>();
        for (int i = 0; i < y.length; ++i) {
            if (!xIDs.remove(y[i]) && !yIDs.contains(y[i])) {
                xIDs.add(y[i]);
            }
            yIDs.add(y[i]);
        }

        return xIDs.iterator().next();
    }
}