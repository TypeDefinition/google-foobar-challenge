import java.math.BigInteger;

public class Solution {
    public static int solution(String x) {
        // Ideally, the best case scenario is when we can do O(log N) operations.
        // To do that, we need to make N a power of 2.
        // Example bit representation of a number: 1111 0101 0101 1111 0101.
        // To turn it into a power of 2, we need to get rid of all the one bits except the MSB.
        int numOps = 0;
        BigInteger numPellets = new BigInteger(x);
        BigInteger three = new BigInteger("3");
        while (!numPellets.equals(BigInteger.ONE)) {
            // Case 1: n is odd.
            if (numPellets.mod(BigInteger.TWO).equals(BigInteger.ONE)) {
                // Is it better to add one or subtract one?
                BigInteger nAddOne = numPellets.add(BigInteger.ONE);
                int lsbAdd = nAddOne.getLowestSetBit();

                BigInteger nSubOne = numPellets.subtract(BigInteger.ONE);
                int lsbSub = nSubOne.getLowestSetBit();

                ++numOps;
                if (lsbAdd > lsbSub && !numPellets.equals(three)) { // When n == 3, there is a special case where it is better to subtract than to add.
                    numPellets = nAddOne;
                } else {
                    numPellets = nSubOne;
                }
            }

            // Case 2: n is even.
            int lowestSetBit = numPellets.getLowestSetBit();
            numOps += lowestSetBit;
            numPellets = numPellets.shiftRight(lowestSetBit);
        }

        return numOps;
    }
}