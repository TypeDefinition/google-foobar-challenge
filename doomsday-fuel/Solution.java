class MathsHelper {
    public static int gcd(int a, int b) {
        return (b == 0) ? a : gcd(b, a % b);
    }

    public static int lcm(int a, int b) {
        return (a / gcd(a, b)) * b;
    }
}

class Fraction {
    public final int n; // Numerator
    public final int d; // Denominator

    public Fraction(int n, int d) {
        int x = MathsHelper.gcd(n, d);
        n /= x;
        d /= x;
        if (n < 0 && d < 0 || n > 0 && d < 0) {
            n = -n;
            d = -d;
        } else if (n == 0 && d < 0) {
            d = -d;
        }
        this.n = n;
        this.d = d;
    }

    public Fraction inverse() {
        return new Fraction(d, n);
    }

    public Fraction add(Fraction frac) {
        return new Fraction(n * frac.d + frac.n * d, d * frac.d);
    }

    public Fraction sub(Fraction frac) {
        return new Fraction(n * frac.d - frac.n * d, d * frac.d);
    }

    public Fraction mult(Fraction frac) {
        return new Fraction(n * frac.n, d * frac.d);
    }

    public Fraction mult(int scalar) {
        return new Fraction(n * scalar, d);
    }

    public Fraction div(Fraction frac) {
        return new Fraction(n * frac.d, d * frac.n);
    }

    public Fraction div(int scalar) {
        return new Fraction(n, d * scalar);
    }

    @Override
    public String toString() {
        return Integer.toString(n) + "/" + Integer.toString(d);
    }

    public float toFloat() {
        return (float) n / (float) d;
    }
}

class Matrix {
    public final int rows, cols;
    public final Fraction[][] values;

    public Matrix(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.values = new Fraction[rows][cols];
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                this.values[i][j] = new Fraction(0, 1);
            }
        }
    }

    public Matrix split(int startRow, int numRows, int startCol, int numCols) {
        Matrix mat = new Matrix(numRows, numCols);
        for (int i = 0; i < numRows; ++i) {
            for (int j = 0; j < numCols; ++j) {
                mat.values[i][j] = values[i + startRow][j + startCol];
            }
        }
        return mat;
    }

    public Matrix minor(int row, int col) {
        Matrix mat = new Matrix(rows - 1, cols - 1);
        int r = 0;
        int c = 0;
        for (int i = 0; i < rows; ++i) {
            if (i == row) {
                continue;
            }
            c = 0;
            for (int j = 0; j < cols; ++j) {
                if (j == col) {
                    continue;
                }
                mat.values[r][c++] = values[i][j];
            }
            ++r;
        }
        return mat;
    }

    public Matrix mult(Matrix mat) {
        Matrix result = new Matrix(rows, mat.cols);
        for (int i = 0; i < mat.cols; ++i) {
            for (int j = 0; j < rows; ++j) {
                for (int k = 0; k < cols; ++k) {
                    result.values[j][i] = result.values[j][i].add(values[j][k].mult(mat.values[k][i]));
                }
            }
        }
        return result;
    }

    public Matrix add(Matrix mat) {
        Matrix result = new Matrix(rows, cols);
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                result.values[i][j] = values[i][j].add(mat.values[i][j]);
            }
        }
        return result;
    }

    public Matrix sub(Matrix mat) {
        Matrix result = new Matrix(rows, cols);
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                result.values[i][j] = values[i][j].sub(mat.values[i][j]);
            }
        }
        return result;
    }

    @Override
    public String toString() {
        String str = new String();
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                str += values[i][j].toString() + " ";
            }
            str += "\n";
        }
        return str;
    }
}

public class Solution {
    static Fraction getDeterminant(Matrix mat) {
        if (mat.rows == 1 && mat.cols == 1) {
            return mat.values[0][0];
        }

        Fraction det = new Fraction(0, 1);
        int sign = 1;
        for (int i = 0; i < mat.rows; ++i) {
            det = det.add(mat.values[0][i].mult(getDeterminant(mat.minor(0, i)).mult(sign)));
            sign = -sign;
        }

        return det;
    }

    static Matrix getAdjugate(Matrix mat) {
        Matrix adjugate = new Matrix(mat.rows, mat.cols);

        if (mat.rows == 1 && mat.cols == 1) {
            adjugate.values[0][0] = new Fraction(1, 1);
        }

        int sign = 1;
        for (int i = 0; i < adjugate.rows; ++i) {
            for (int j = 0; j < adjugate.cols; ++j) {
                sign = ((i + j) % 2 == 0) ? 1 : -1;
                adjugate.values[j][i] = getDeterminant(mat.minor(i, j)).mult(sign);
            }
        }

        return adjugate;
    }

    static Matrix getInverse(Matrix mat) {
        Fraction det = getDeterminant(mat);
        Matrix inv = getAdjugate(mat);

        // Find inverse using formula "inv(M) = adj(M)/det(M)".
        for (int i = 0; i < inv.rows; i++) {
            for (int j = 0; j < inv.cols; j++) {
                inv.values[i][j] = inv.values[i][j].div(det);
            }
        }

        return inv;
    }

    static Matrix getIdentity(int size) {
        Matrix identity = new Matrix(size, size);
        for (int i = 0; i < size; ++i) {
            identity.values[i][i] = new Fraction(1, 1);
        }
        return identity;
    }

    // Check out my portfolio! https://lnxterry.portfolio.site/
    public static int[] solution(int[][] m) {
        // Edge case.
        if (m.length == 1) {
            int[] edgeCase = new int[2];
            edgeCase[0] = 1;
            edgeCase[1] = 1;
            return edgeCase;
        }

        // Convert m into the standard form for an absorbing markov chain.
        Matrix mat = new Matrix(m.length, m.length);
        int[] newOrder = new int[m.length];
        int head = 0;
        int tail = m.length;

        // Generate the adjacency matrix.
        for (int i = 0; i < mat.rows; ++i) {
            // Calculate the denominator.
            int sum = 0;
            for (int j = 0; j < mat.cols; ++j) {
                sum += m[i][j];
            }

            // If this is a terminating state, it has a 1/1 chance of returning to itself
            // the next cycle.
            if (sum == 0) {
                mat.values[i][i] = new Fraction(1, 1);
                newOrder[--tail] = i; // Put absorbing states behind.
                continue;
            }

            for (int j = 0; j < mat.rows; ++j) {
                mat.values[i][j] = new Fraction(m[i][j], sum);
            }
            newOrder[head++] = i; // Put transient states in front.
        }

        // Reorder the matrix into the standard Markov Chain matrix form.
        Matrix standardMat = new Matrix(m.length, m.length);
        for (int i = 0; i < standardMat.rows; ++i) {
            for (int j = 0; j < standardMat.cols; ++j) {
                standardMat.values[i][j] = mat.values[newOrder[i]][newOrder[j]];
            }
        }

        //                   | Q | R |
        // Standard Matrix = |-------|
        //                   | 0 | I |
        int numTransient = head;
        int numAbsorbing = m.length - head;
        Matrix q = standardMat.split(0, numTransient, 0, numTransient);
        Matrix r = standardMat.split(0, numTransient, numTransient, numAbsorbing);
        Matrix f = getInverse(getIdentity(head).sub(q)); // Fundamental matrix.
        Matrix fr = f.mult(r);

        // s0 is always gauranteed to be a transient state, and will always be at the front.
        Matrix s0 = new Matrix(1, numTransient);
        s0.values[0][0] = new Fraction(1, 1);

        // The probability of terminating from s0.
        Matrix probabilityMat = s0.mult(fr);

        // Find the common denominator.
        int commonDenom = 1;
        for (int i = 0; i < probabilityMat.cols; ++i) {
            commonDenom = MathsHelper.lcm(commonDenom, probabilityMat.values[0][i].d);
        }

        // Get the results.
        int[] results = new int[numAbsorbing + 1];
        results[numAbsorbing] = commonDenom;
        for (int i = 0; i < probabilityMat.cols; ++i) {
            results[numAbsorbing - 1 - i] = probabilityMat.values[0][i].n * (commonDenom/probabilityMat.values[0][i].d);
        }

        return results;
    }
}