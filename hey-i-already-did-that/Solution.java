public class Solution {
    private static String iterate(String n, int b) {
        char[] yArr = n.toCharArray();
        char[] xArr = new char[yArr.length];

        java.util.Arrays.sort(yArr);
        for (int j = 0; j < yArr.length; ++j) {
            xArr[j] = yArr[yArr.length - 1 - j];
        }

        int y = Integer.parseInt(new String(yArr), b);
        int x = Integer.parseInt(new String(xArr), b);

        return Integer.toString(x - y, b);
    }

    public static int solution(String n, int b) {
        int k = n.length();
        int counter = 0;

        String u = n;
        for (int i = 0; i < k; ++i) {
            u = iterate(u, b);
        }

        String v = u;
        while (true) {
            v = iterate(v, b);
            ++counter;
            if (v.equals(u)) {
                break;
            }
        }

        return counter;
    }
}